There are two Prism directories for code highlight:
1) Prism: 
  * This is for the prism class on OUTPUT. It will syntax highlight to drupal.org standards
2) Prism-Ckeditor:
  * This is for the code snippet ckeditor styles. 
  * It is nearly the same, with some Javascript changes to work better in CKEditor.
  * If you wish to test un-minified code out, you must change plugin.js
