; This make file appends the contrib folder to each common module
; Please make sure to add a line here if you add any modules to drupal.org-make.
projects[security_review][subdir] = "contrib"
projects[paranoia][subdir] = "contrib"
projects[drupalorg_crosssite][subdir] = "contrib"
projects[bakery][subdir] = "contrib"

; Since bluecheese is d.o specific, we place it in the custom folder.
projects[bluecheese][subdir] = "custom"
