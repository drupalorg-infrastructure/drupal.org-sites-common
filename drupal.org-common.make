;; Common for Drupal.org D7 sites.

;; Security Modules
projects[security_review][version] = "1.2"

projects[paranoia][version] = "1.6"

projects[drupalorg_crosssite][version] = "3.x-dev"
projects[drupalorg_crosssite][download][revision] = "81f4c7c"

projects[bakery][version] = "2.x-dev"
projects[bakery][download][revision] = "35d3742"

projects[composer_manager][version] = "1.x-dev"
; Unnecessary folder write permission requirement causes WSOD
; https://www.drupal.org/node/2620348#comment-11631561
projects[composer_manager][download][revision] = "d0f3eb4"
projects[composer_manager][patch][3182422] = "https://www.drupal.org/files/issues/2021-02-27/composer_manager-composer_2_packages-3182422-7-D7.patch"

;; Drupal.org Theme
projects[bluecheese][type] = "theme"
projects[bluecheese][download][type] = "git"
projects[bluecheese][download][url] = "git@gitlab.com:drupal-infrastructure/sites/bluecheese-private.git"
projects[bluecheese][download][branch] = "branded-2.x"
