;; Overrides for Drupal.org dev sites.

projects[devel][version] = "1.7"

projects[maillog][version] = "1.0-alpha1"

projects[solr_devel][version] = "1.0-alpha4"


; Reset Bluecheese to download from Drupal.org.
projects[bluecheese][type] = "theme"
projects[bluecheese][version] = "2.x"
projects[bluecheese][download] = ""
